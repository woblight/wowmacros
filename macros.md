# WoW 1.12 Macros

Most of these macros require SuperMacro or other means of loading the **Extended Lua** code in global scope.

# Extended Lua

This code doesn't have any dependency, macros will work as long as this code somehow loaded.

If you're using SuperMacro ofr this, it's advised to make a single empty macro and copy/paste the extended lua in it, as SuperMacro extended code is shared among all macros (it's loaded in globa scope).

### Exteneded Lua (Part 1)
```lua
function GetSpellID(sn)
    sn = strlower(sn)
    
    local i,a,r,f = 1, GetSpellName(1,BOOKTYPE_SPELL)
    while a do
        if strlower(format("%s(%s)", a, r or "")) == sn then
            return i
        elseif strlower(a) == sn then
            f = i
        elseif f then
            return f
        end
        i = i+1
        a,r = GetSpellName(i,BOOKTYPE_SPELL)
    end
    return f
end

function IsActive(sn)
  local _, _, active = GetSpellCooldown(GetSpellID(sn),"spell")
  return active == 0
end

function IsSpellOnCD(sn)
 return GetSpellCooldown(GetSpellID(sn),"spell")~=0
end

function GetActiveForm()
  for i=1,GetNumShapeshiftForms() do
    if  ({GetShapeshiftFormInfo(i)})[3]
    then return i end
  end
  return 0
end

function CancelForm()
  local f = GetActiveForm()
  if f ~= 0 then
    CastShapeshiftForm(f)
  end
end

function toString(a, loop, depth)
  loop = loop or {}
    if a == nil then return "nil" end
    if a == true then return "TRUE" end
    if a == false then return "FALSE" end
    if type(a) == "string" then return "\""..a.."\"" end
    if type(a) == "table" then
        local address = tostring(a)
        if loop[address] or depth == 0 then
            return address
        else
            loop[address] = true
        end
        local out = format("%s {", a.GetName and a:GetName() or address)
        local sep = ""
        for k,v in a do
            out = out..sep..toString(k, loop, depth -1)..": "..toString(v, loop, depth -1)
            sep = ", "
        end
        return out.."}"
    end
    return tostring(a)
end
function debug(a,depth)
  DEFAULT_CHAT_FRAME:AddMessage(toString(a, nil, depth or  -1))
end

function wait (t, fun, n,  ...)
  if Timer == nil then
    Timer = CreateFrame("frame")
    Timer.timers = {}
    Timer.lastUpdate = GetTime()
    Timer:SetScript("OnUpdate",
      function()
        elapsed = GetTime() - Timer.lastUpdate
        for k,timer in Timer.timers do
          timer.e = timer.e + elapsed
          if timer.t < timer.e then
            if timer.fun(unpack(timer.args)) ~=true and timer.n ~= 0 then
              timer.e = timer.e - timer.t
              timer.n = timer.n -1
            else
              table.remove(Timer.timers,k)
            end
          end
        end
        Timer.lastUpdate = GetTime()
      end
    )
  end
  table.insert(Timer.timers,{t=t,fun=fun,e=0,n=(n and n -1 or 0),args=arg})
end

function HasBuffNamed(unit, name,other)
    if BuffScanner == nil then
        CreateFrame("GameTooltip","BuffScanner",UIParent,"GameTooltipTemplate")
    end
    for i = 1,16 do
        BuffScanner:SetOwner(UIParent,"ANCHOR_NONE")
        BuffScanner:SetUnitBuff(unit,i)
        local bn = BuffScannerTextLeft1:GetText()
        if bn and bn == name or bn == other then
            return true
        end
    end
    return false
end

function RaidBuff(name,other, classes)
    if name == nil or name == "" then return end
    if other == nil then other = "" end
    local num = GetNumRaidMembers()
    local root = "raid"
    if num == 0 then
        num = GetNumPartyMembers()
        root = "party"
    end
    if num == 0 then return end
    if RaidBufferLast == nil then
        RaidBufferLast = 0
    end
    for i = RaidBufferLast,RaidBufferLast + num do
        local unit = root .. math.mod(i, num) + 1
        if (not classes or classes[UnitClass(unit)]) and not HasBuffNamed(unit, name,other) then
             local re = UnitExists("target")
            local sc = GetCVar("autoSelfCast")
            if sc == "1" then
                SetCVar("autoSelfCast",0)
            end
            ClearTarget()
            CastSpellByName(name)
            if sc == "1" then
                SetCVar("autoSelfCast",1)
            end
            if not SpellIsTargeting() then
                if re then TargetLastTarget() end
                return false
            end
            local can = SpellCanTargetUnit(unit)
            if can then
                SpellTargetUnit(unit)
                RaidBufferLast = math.mod(i, num) +1
                if re then
                    TargetLastTarget()
                end
                return false
            else
                SpellStopTargeting()
                if re then
                    TargetLastTarget()
                end
            end
        end
    end
    return true
end
```

### Extended Lua (Part 2)

```lua
-- spell fallback

function fallback(a, f)
    local old = UIErrorsFrame.AddMessage
    function UIErrorsFrame:AddMessage(message, ...)
        local s = f[message]
        if s then
            s()
        else
            old(UIErrorsFrame, message, unpack(arg))
        end
    end
    a()
    UIErrorsFrame.AddMessage = old
end

-- Chat Countdown

if CountdownMacro == nil then
    CountdownMacro = CreateFrame("frame")
    CountdownMacro.cds = {}
end

local function update()
    for k,cd in this.cds do
        cd.timer = cd.timer + arg1
        if cd.timer >= 1 then
            if cd.n == 0 then
                SendChatMessage(cd.final,cd.chat,nil,cd.target)
            else
                SendChatMessage(format(cd.text,cd.n),cd.chat,nil,cd.target)
            end
            cd.timer = 1 - cd.timer
            cd.n = cd.n - 1
            if cd.n < 0 then
                this.cds[k] = nil
            end
        end
    end
end

CountdownMacro:SetScript("OnUpdate", update)

function CountdownMacro:Start(name, text, final, chat, counts, target)
    self.cds[name] = {text = text, final = final, chat = chat, n = counts, target = target, timer = 1}
end

function CountdownMacro:Stop(name, aborted, chat, target)
    self.cds[name] = nil
    SendChatMessage(aborted,chat,nil,target)
end

function CountdownMacro:Toggle(name, text, final, aborted, chat, counts, target)
    if self.cds[name] then
        self:Stop(name, aborted, chat, target)
    else
        self:Start(name, text, final, chat, counts, target)
    end
end

-- mouse over cast for friendly spells

function MouseOverCast(name)
    local re = UnitExists("target")
    local sc = GetCVar("autoSelfCast")
    if sc == "1" then
        SetCVar("autoSelfCast",0)
    end
    ClearTarget()
    CastSpellByName(name)
    if sc == "1" then
        SetCVar("autoSelfCast",1)
    end
    if not SpellIsTargeting() then
        if re then TargetLastTarget() end
        return
    end
    local done = false
    for _,unit in {"mouseover","mouseovertarget"} do
        if SpellCanTargetUnit(unit) then
            SpellTargetUnit(unit)
            done = true
        end
    end
    if re then TargetLastTarget() end
    if done then
        return
    elseif not SpellIsTargeting() then
        TargetLastTarget()
    end
    if re and UnitExists("targettarget") and SpellCanTargetUnit("targettarget") then
        SpellTargetUnit("targettarget")
    elseif SpellCanTargetUnit("player") then
        SpellTargetUnit("player")
    end
    SpellStopTargeting()
end

-- melee spell fallback

function MeleeFallback(a,b)
  if
    fails(function()
      CastSpellByName(a)
      end,{SPELL_FAILED_TOO_CLOSE,SPELL_FAILED_NEED_AMMO}) then
      CastSpellByName(b)
  end
end

-- combat spell fallack

function CombatFallback(a)
  if
    fails(function()
      if not IsActive(a) then CastSpellByName(a) end
      end,{SPELL_FAILED_AFFECTING_COMBAT}) then
      if not catchError($->UseAction(6)) then
          PetFollow()
      end
  end
end

function catchError(a)
    local e
    local old = UIErrorsFrame.AddMessage
    function UIErrorsFrame:AddMessage(message, ...)
        e = message
        if not e then
            old(UIErrorsFrame, message, unpack(arg))
        end
    end
    a()
    UIErrorsFrame.AddMessage = old
    return e
end
```

### Auto Unshift/Dismount (extended lua only)

```lua
-- Auto Unshift/Dismount

if not AutoUnshift then
    AutoUnshift = {sendError = UIErrorsFrame.AddMessage}
    function UIErrorsFrame:AddMessage(...)
        for _,s in {
            ERR_CANT_INTERACT_SHAPESHIFTED,
            SPELL_FAILED_NOT_SHAPESHIFT,
            SPELL_FAILED_NO_ITEMS_WHILE_SHAPESHIFTED,
            SPELL_NOT_SHAPESHIFTED,
            SPELL_NOT_SHAPESHIFTED_NOSPACE,
            ERR_EMBLEMERROR_NOTABARDGEOSET,
            ERR_MOUNT_SHAPESHIFTED,
            ERR_NOT_WHILE_SHAPESHIFTED,
            ERR_NO_ITEMS_WHILE_SHAPESHIFTED,
            ERR_TAXIPLAYERSHAPESHIFTED
        } do
            if strfind(arg[1],s,nil,true) then
                CancelForm()
                return
            end
        end
        if not AutoUnshift.mount then
            for _,s in {
                SPELL_FAILED_NOT_MOUNTED,
                ERR_TAXIPLAYERALREADYMOUNTED
            } do
                if strfind(arg[1],s,nil,true) then
                    AutoUnshift.mount = true
                    UseAction(36)
                    AutoUnshift.mount = nil
                    return
                end
            end
        end
        AutoUnshift.sendError(self,unpack(arg))
    end
end
```

### auto ammo swap (extended lua only)

```lua
-- auto ammo swap

ArrowSwapper = ArrowSwapper or CreateFrame("frame")
ArrowSwapper:RegisterEvent("PLAYER_TARGET_CHANGED")
ArrowSwapper:SetScript("OnEvent", function ()
    if UnitIsDeadOrGhost("player") or not UnitIsEnemy("player","target") then return end
    if UnitIsPlayer("target") and GetInventoryItemTexture("player", 0) ~= GetActionTexture(16) then
        UseAction(16)
    end
    if not UnitIsPlayer("target") and GetInventoryItemTexture("player", 0) ~= GetActionTexture(15) then
        UseAction(15)
    end
end)
```
# Macros
## General Macros

### countdown
```
/run CountdownMacro:Toggle("pull","Pulling in %d secs!","=== PULLING ===","ABORTED","SAY",5)
```

### set player dots alpha in map equal to their health % (for battlegrounds)
```
/run for _,p in {"BattlefieldMinimap","WorldMap"}do for i=1,40 do local n = i  local f=getglobal(p.."Raid"..i) f:SetScript("OnUpdate",function() f:SetAlpha(UnitHealth("raid"..n)/UnitHealthMax("raid"..n)) end) end end
```

### click: stores player position and calculate mean with previous positions; alt + click: reset; ctrl+click print medium pos in chat.
```
/run if P == nil then P = {GetPlayerMapPosition("player")} n = 1 elseif IsAltKeyDown() then P = nil elseif IsControlKeyDown() then debug({P[1]/n,P[2]/n}) else local x,y = GetPlayerMapPosition("player") P = {P[1] + x, P[2] + y} n = n + 1 end
```

## HUNTER MACROS

### Arcane Shot / Raptor Strike 
```
/run MeleeFallback("Arcane Shot", "Raptor Strike")
```
### frost trap / feign death
```
/run CombatFallback("Frost Trap")
```

## DRUID MACROS

### Cancel all forms
```
/run CancelForm()
```
### Cast Mark of the Wild on raid/party if missing (Hold ctrl down for Thorns)
```
/run if RaidBuff("Mark of the Wild","Gift of the Wild") and IsControlKeyDown() then RaidBuff("Thorns","") end
```
### Pounce/Claw
```
/run if not IsActive("Prowl") then CastSpellByName("Claw") else CastSpellByName("Pounce") end
```
### Safe prowl
```
/run if not IsActive("Prowl") then CastSpellByName("Prowl")end
```
### Ravage/Shred
```
/run if not IsActive("Prowl") then CastSpellByName("Shred") else CastSpellByName("Ravage") end
```
### Safe shadowmeld
```
/run if not IsActive("Shadowmeld") then CastSpellByName("Shadowmeld")end
```
### Any -> Bear
```
/run local a=GetActiveForm() if a==0 then CastShapeshiftForm(1) else if a ~= 1 then CastShapeshiftForm(a) end end 
```
### Cat Cat Cat Cat Cat ...
```
/run if IsActive('Prowl') then CastSpellByName("Prowl") else local a=GetActiveForm() if a==0 then CastShapeshiftForm(3) else if not IsSpellOnCD('Cat Form') then CastShapeshiftForm(a)end end end
```
### Travel/Aquatic form
```
/run if  not IsSpellOnCD('Travel Form') then if GetActiveForm() == 0 then if fails(function()CastShapeshiftForm(4)end,{SPELL_FAILED_ONLY_ABOVEWATER})then CastShapeshiftForm(2)end else CancelForm() end end
```
## MAGE

### RAID BUFF
```
/run RaidBuff("Arcane Intellect", "Arcane Brilliance", {Paladin=true, Druid=true, Mage=true, Hunter=true, Priest=true, Warlock=true})
```
